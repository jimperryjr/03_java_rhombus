package edu.westga.cs6311.midterm.model;

import java.awt.geom.Point2D;

/**
 * MidTerm Part1 
 * This is the driver for the informal test application
 * 
 * @author jim perry, jr.
 * @version 10.02.2015
 * 
 **/
public class Rhombus {
	private Point2D.Double topPoint;	
	private double lengthOfHorizontalDiagonal;
	private double lengthOfVertical;
	
	/**
	 * initialize the instance variable(s).
	 */
	public Rhombus() {
		this.topPoint = new Point2D.Double(0, 0);	
		this.lengthOfHorizontalDiagonal = 0.0;
		this.lengthOfVertical = 0.0;			
	}
	
	/**
	 * defines a new rhombus
	 * @param xCordinate The point that defines the (x) coordinate of the top point
	 * @param yCordinate The point that defines the (y) coordinate of the top point
	 * @param lengthHorizontalDiagonal The length of the horizontal diagonal (left point to right point)
     * @param lengthVertical The length of the vertical diagonal (top point to bottom point)
	 */		
	public Rhombus(double xCordinate, double yCordinate, double lengthHorizontalDiagonal, double lengthVertical) {
		this.topPoint = new Point2D.Double(xCordinate, yCordinate);	
		this.lengthOfHorizontalDiagonal = lengthHorizontalDiagonal;
		this.lengthOfVertical = lengthVertical;	
	}
	
	/**
	 *  Gets the location of rhombus' topPoint
	 * @return returns (x,y) coordinates of rhombus' top point 
	 */	
	public Point2D.Double getTopPoint() {
		return this.topPoint;				 
	}
	
	/**
	 * Gets the location of rhombus' bottom point
	 * @return returns (x,y) coordinates of rhombus' bottom point 
	 */		
	public Point2D.Double getBottomPoint() {
		return new Point2D.Double(
				this.topPoint.getX(), 
				this.topPoint.getY() + this.lengthOfVertical
		);
	};
	
	/**
	 * Gets the location of rhombus' right point
	 * @return returns (x,y) coordinates of rhombus' right point 
	 */		
	public Point2D.Double getLeftPoint() {
		return new Point2D.Double(
				this.topPoint.getX() - (this.lengthOfHorizontalDiagonal / 2), 
				this.topPoint.getY() + (this.lengthOfVertical / 2)
		);		
	};
	
	/**
	 * Calculates location of rhombus' right point
	 * @return returns (x,y) coordinates of rhombus' right point 
	 */		
	public Point2D.Double getRightPoint() {
		return new Point2D.Double(
				this.topPoint.getX() + (this.lengthOfHorizontalDiagonal / 2), 
				this.topPoint.getY() + (this.lengthOfVertical / 2)
		);
	};
	
	/**
	 * Calculates the length of horizontal diagonal of rhombus
	 * @return returns length of horizontal diagonal of rhombus 
	 */		
	public double getHorizontalLength() {
		return this.lengthOfHorizontalDiagonal;
	};
	
	/**
	 * Calculates the vertical length of rhombus
	 * @return returns length of the vertical of rhombus 
	 */		
	public double getVerticalLength() {
		return this.lengthOfVertical; 
	};
	
	/**
	 * Calculates the area of rhombus.
	 * @return returns the area of rhombus. 
	 */		
	public double getArea() {		
		return ((0.5 * this.lengthOfHorizontalDiagonal) * this.lengthOfVertical);		
	};
	
	/**
	 * Calculates the length of each side of rhombus.
	 * @return returns the side length of rhombus. 
	 */	
	public double getSideLength() {		
		return Math.sqrt((Math.pow(this.lengthOfHorizontalDiagonal, 2) + Math.pow(this.lengthOfVertical, 2)) / 4);
	};
	
	
	/**
	 * Calculates the perimeter of rhombus.
	 * @return returns the perimeter of rhombus. 
	 */		
	public double getPerimeter() {
		return 4 * this.getSideLength();		
	};
	
	/**
	 * Calculates ratio between area of rhombus and rectangle;
	 * rectangle sides are equal to rhombus's sides length
	 * @return returns the ratio of area of rhombus 
	 */	
	public double getRatioOfAreaToSquareArea() {
		return this.getArea() / Math.round(Math.pow(this.getSideLength(), 2));
	};
	
	/**
	 * Calculates of radius' inscribed circle' 
	 * @return returns radius of circle inside rhombus
	 */	
	public double getInradius() {				
		return (this.lengthOfHorizontalDiagonal * this.lengthOfVertical) / Math.sqrt(Math.pow(this.lengthOfHorizontalDiagonal, 2) + Math.pow(this.lengthOfVertical, 2));
	};		

}
