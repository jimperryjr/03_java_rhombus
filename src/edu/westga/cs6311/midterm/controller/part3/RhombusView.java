package edu.westga.cs6311.midterm.controller.part3;

import edu.westga.cs6311.midterm.model.Rhombus;
import java.util.Scanner; 

/**
 * MidTerm Part3 
 * This is the class to gives the user the ability to 
 * create a Rhombus using their own values for (x, y), 
 * vertical and horizontal lengths
 * 
 * @author jim perry, jr.
 * @version 10.02.2015
 * 
 **/
public class RhombusView {
	private Scanner userKeyboard;
	private Rhombus shapeRhombus;
	
	private double lengthOfHorizontalDiagonal;
	private double lengthOfVertical;	
	private double xTopPoint;
	private double yTopPoint;
		
	
	/**
	 * initialize the instance variable(s).
	 */
	public RhombusView() {
		this.userKeyboard = new Scanner(System.in);

		this.lengthOfHorizontalDiagonal = 0.0;
		this.lengthOfVertical = 0.0;
		this.xTopPoint = 0.0;
		this.yTopPoint = 0.0;
		
		this.shapeRhombus = null;
	}

	/**
	 * captures keyboard input parameters to create a rhombus
	 */	
	public void inputRhombusValues() {
		System.out.println("Midterm Part 3: \n");
		
		System.out.println("Please enter x cordinate for rhombus: ");
		this.xTopPoint = Double.parseDouble(this.userKeyboard.nextLine());	
		
		System.out.println("Please enter y cordinate for rhombus: ");	
		this.yTopPoint = Double.parseDouble(this.userKeyboard.nextLine());
		
		System.out.println("Please enter rhombus' horizontal diagonal length: ");
		this.lengthOfHorizontalDiagonal = Double.parseDouble(this.userKeyboard.nextLine());
		
		System.out.println("Please enter rhombus' vertical diagonal length: ");
		this.lengthOfVertical = Double.parseDouble(this.userKeyboard.nextLine());				
	}	
	
	/**
	 * initialize the rhombus with user input
	 */
	public void initializeRhombus() {
		this.shapeRhombus = new Rhombus(this.xTopPoint, this.yTopPoint, this.lengthOfHorizontalDiagonal, this.lengthOfVertical);
	}
	
	
	/**
	 * Helper method to display a message about the attributes of rhombus
	 */
	public void demoRhombus() {		
		System.out.println("Your input value for top point value");
		System.out.println("\tActual x:  " + this.shapeRhombus.getTopPoint().getX() + "\t\tActual y:  " + this.shapeRhombus.getTopPoint().getY() + "\n"); 
		
		System.out.println("Your horizontal diagonal length value: ");
		System.out.println("\tActual horizontal length:  " + this.shapeRhombus.getHorizontalLength() + "\n"); 		
		
		System.out.println("Your vertical diagonal length value: ");
		System.out.println("\tActual vertical length:  " + this.shapeRhombus.getVerticalLength() + "\n\n");
		
		System.out.println("Additional rhombus attributes: ");

		System.out.println("\trhombus top point:                   " + this.shapeRhombus.getTopPoint().getX() + ", " + this.shapeRhombus.getTopPoint().getY()); 
		System.out.println("\trhombus bottom point:                " + this.shapeRhombus.getBottomPoint().getX() + ", " + this.shapeRhombus.getBottomPoint().getY()); 
		System.out.println("\trhombus left point:                  " + this.shapeRhombus.getLeftPoint().getX() + ", " + this.shapeRhombus.getLeftPoint().getY()); 
		System.out.println("\trhombus right point:                 " + this.shapeRhombus.getRightPoint().getX() + ", " + this.shapeRhombus.getRightPoint().getY()); 
		
		
		System.out.println("\trhombus perimeter:                   " + Math.round(this.shapeRhombus.getPerimeter())); 				
		System.out.println("\trhombus area:                        " + this.shapeRhombus.getArea()); 		
		System.out.println("\trhombus side length:                 " + Math.round(this.shapeRhombus.getSideLength()));				
		System.out.println("\trhombus perimeter value:             " + Math.round(this.shapeRhombus.getPerimeter())); 				
		System.out.println("\trhombus area to square area ratio:   " + this.shapeRhombus.getRatioOfAreaToSquareArea()); 		
		System.out.println("\trhombus inscribed' circle area:      " + Math.round(this.shapeRhombus.getInradius()));		
	}


}
