package edu.westga.cs6311.midterm.controller.part3;

/**
 * MidTerm Part3 
 * This is the driver for the informal test application
 * 
 * @author jim perry, jr.
 * @version 10.02.2015
 * 
 **/
public class RhombusDriver {

	/**
	 * This method is the entry point of the application
	 * @param args Command-line arguments, not used
	 */ 
	public static void main(String[] args) {
		RhombusView demo = new RhombusView();
		demo.inputRhombusValues();
		demo.initializeRhombus();
		demo.demoRhombus();

	}

}
