package edu.westga.cs6311.midterm.controller.test;

/**
 * MidTerm Part1 
 * This is the driver for the informal test application
 * 
 * @author jim perry, jr.
 * @version 10.02.2015
 * 
 **/
public class TestDriver {
	
	/**
	 * This method is the entry point of the application
	 * @param args Command-line arguments, not used
	 */ 
	public static void main(String[] args) {   
		RhombusDemo demo = new RhombusDemo();
		demo.testRhombusPart01();
		demo.testRhombusPart02();
	}

}
