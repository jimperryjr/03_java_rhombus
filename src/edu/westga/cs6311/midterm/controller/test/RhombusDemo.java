package edu.westga.cs6311.midterm.controller.test;

import java.awt.geom.Point2D;
import edu.westga.cs6311.midterm.model.Rhombus;

/**
 * MidTerm Part1 
 * This class is used to informally test the RhombusDemo class.
 * 
 * @author jim perry, jr.
 * @version 10.02.2015
 * 
 **/
public class RhombusDemo {
	private static final Point2D.Double DEFINE_TOP_POINT = new Point2D.Double(10, 20);
	private static final double DEFINE_HORIZONTAL_LENGTH = 40;
	private static final double DEFINE_VERTICAL_LENGTH = 40; 	
	
	private static final Point2D.Double EXPECTED_BOTTOM_POINT = new Point2D.Double(10, 60);
	private static final Point2D.Double EXPECTED_LEFT_POINT = new Point2D.Double(-10, 40);	
	private static final Point2D.Double EXPECTED_RIGHT_POINT = new Point2D.Double(30, 40);

	private static final double EXPECTED_AREA = 800;
	private static final double EXPECTED_SIDE_LENGTH = 28.28427;
	
	private static final double PERIMETER = 113.137;
	private static final double INRADIUS = 28.28427;
	private static final double RATIO = 0.99; 	

	private Rhombus rhombusShape;	
	
	/**
	 * Creates a new RhombusDemo object with constants 
	 *  as arguments for the new rhombus
	 */
	public RhombusDemo() {
		this.rhombusShape = new Rhombus(DEFINE_TOP_POINT.getX(), DEFINE_TOP_POINT.getY(), DEFINE_HORIZONTAL_LENGTH, DEFINE_VERTICAL_LENGTH);
	}

	/**
	 * Executes tests of rhombus behavior.
	 */	
	public void testRhombusPart01() {	
		System.out.println("Midterm Part 1: \n");
		System.out.println("Testing for the top point value");
		this.displayPointInformation(this.rhombusShape.getTopPoint(), DEFINE_TOP_POINT.getX(), DEFINE_TOP_POINT.getY());		
		
		System.out.println("Testing for the bottom point value");
		this.displayPointInformation(this.rhombusShape.getBottomPoint(), EXPECTED_BOTTOM_POINT.getX(), EXPECTED_BOTTOM_POINT.getY());		
		
		System.out.println("Testing for the left point value");
		this.displayPointInformation(this.rhombusShape.getLeftPoint(), EXPECTED_LEFT_POINT.getX(), EXPECTED_LEFT_POINT.getY());		
		
		System.out.println("Testing for the right point value");
		this.displayPointInformation(this.rhombusShape.getRightPoint(), EXPECTED_RIGHT_POINT.getX(), EXPECTED_RIGHT_POINT.getY());		
		
		System.out.println("Testing for the horizontal length value");
		this.displayInformation(this.rhombusShape.getHorizontalLength(), DEFINE_HORIZONTAL_LENGTH);
		
		System.out.println("Testing for the vertical length value");
		this.displayInformation(this.rhombusShape.getVerticalLength(), DEFINE_VERTICAL_LENGTH);
		
		System.out.println("Testing for the area value");
		this.displayInformation(this.rhombusShape.getArea(), EXPECTED_AREA);		
	
		System.out.println("Testing for the side length value");
		this.displayInformation(this.rhombusShape.getSideLength(), EXPECTED_SIDE_LENGTH);		
	}

	/**
	 * Executes additional tests of rhombus behavior.
	 */	
	public void testRhombusPart02() {	
		System.out.println("\n\nMidterm Part 2: \n");
		System.out.println("Testing for the perimeter value");
		this.displayInformation(this.rhombusShape.getPerimeter(), PERIMETER);		
	
		System.out.println("Testing ratio of area to square area value");
		this.displayInformation(this.rhombusShape.getRatioOfAreaToSquareArea(), RATIO);		
	
		System.out.println("Testing for 'inscribed' circle area value");
		this.displayInformation(this.rhombusShape.getInradius(), INRADIUS);		
	
	}
	
	/**
	 * Helper method to display a message and the expected attributes of rhombus
	 *
	 * @param actualPoint The actual point (x, y) of the top point of rhombus
	 * @param expectedX	The expected (x) coordinate of the top point of rhombus 
	 * @param expectedY	The expected (y) coordinate of the top point of rhombus 
	 */
	public void displayPointInformation(Point2D.Double actualPoint, double expectedX, double expectedY) {
		System.out.println("\tExpected x:\t" + expectedX + "\tExpected y:\t" + expectedY);
		System.out.println("\tActual x:\t" + actualPoint.getX() + "\tActual y:\t" + actualPoint.getY() + "\n"); 
	}
	
	/**
	 * Helper method to display a message and the expected attributes of Rhombus
	 *
	 * @param expected	The expected value of a property of rhombus object 
	 * @param actual	The actual value of rhombus' attribute 
	 */	
	public void displayInformation(double actual, double expected) {
		System.out.println("\tExpected value:\t" + expected + "\tActual value:\t" + actual  + "\n");
	}	
	
}
